//
//  MapForItenerary.m
//  GitSourceControl
//
//  Created by Sparity Mini-2 on 25/11/13.
//  Copyright (c) 2013 sparity. All rights reserved.
//

#import "MapForItenerary.h"

@implementation MapForItenerary

- (void)fakeMethod
{
    // This is a fake method just to make the discard action clear for you
    NSLog(@" Discarding changes allow you to get the latest revision you have worked on it.");
}

@end
