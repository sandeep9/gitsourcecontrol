//
//  main.m
//  GitSourceControl
//
//  Created by Sparity Mini-2 on 25/11/13.
//  Copyright (c) 2013 sparity. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
