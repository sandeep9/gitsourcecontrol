//
//  ViewController.m
//  GitSourceControl
//
//  Created by Sparity Mini-2 on 25/11/13.
//  Copyright (c) 2013 sparity. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBActions

- (IBAction)clickTheButton:(id)sender
{
    NSLog(@"This is a Git tutorial");
}

@end
