//
//  AppDelegate.h
//  GitSourceControl
//
//  Created by Sparity Mini-2 on 25/11/13.
//  Copyright (c) 2013 sparity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
